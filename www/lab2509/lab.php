<!DOCTYPE html>
<html>

<body>
  <script>
   /* 'use strict';
    let user = {
      name: "John",
      age: 30
    };
    
    let key = prompt("What do you want to know about the user?", "name");
    
    // access by variable
    alert( user[key] ); // John (if enter "name")
	function makeUser(name, age) {
  	return {
  	 name: name,
  	 age: age
    // ...other properties
  	};
	}

	let user = makeUser("John", 30);
	alert(user.name); // John*/

	// explicit conversion
	let str = `Hello`;

// the first character
/*alert( str[0] ); // H
alert( str.charAt(0) ); // H

// the last character
alert( str[str.length - 1] ); // o

*/
	let str = 'Hi';

	str[0] = 'h'; // error
	alert( str[0] );
  </script>
</body>

</html>